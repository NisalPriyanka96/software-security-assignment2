from flask import *
import driver
import profile
from cryptography import x509
import profile
import os
from werkzeug.utils import secure_filename


app = Flask(__name__)

## upload
app.config['UPLOAD_FOLDER']='upload'
app.config['MAX_CONTENT_PATH'] = 734003200 ## 700 mb allowed

app.add_url_rule('/', view_func=driver.root)
app.add_url_rule('/driver/redirect', view_func=driver.redirectToGoogle)

## getting code
app.add_url_rule('/driver/callback', view_func=driver.getCode)

## redirect to access token & get user info
app.add_url_rule('/driver/getToken', view_func=driver.getAccessToken)

## redirect to dashboard
app.add_url_rule('/api/user/dashboard', view_func=driver.getDashBoard)

## get user image
app.add_url_rule('/api/user/profile/image', view_func=profile.getProfileImage, methods=["POST"])

## get news
app.add_url_rule('/api/user/newsTop', view_func=profile.getNewsTop, methods=["GET"])

## upload file to drive
app.add_url_rule('/api/user/driver/upload', view_func=profile.upload_file, methods=["POST"])

## upload file to server
@app.route('/api/user/server/upload' , methods=['POST'])
def serverUpload():
    if request.method == 'POST':
        # print(request.form.get('file'))
        
        if 'file' not in request.files:
            return "No File Part"
        
        f = request.files['file']
        
        if f.filename == '':
                return 'No Selected File'
        
        f.save(os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(f.filename)))

        token = request.form.get('token')
        filepath = os.getcwd()+"/upload/"+f.filename

        print(token+" "+filepath)

        profile.upload_file(token, filepath, f.filename)

        return 'File Upload Successfully'

        #profile.upload_file(token, filepath)

    return ''

## test endpoint
app.add_url_rule('/test', view_func=driver.test)
    

if __name__ == '__main__':
    app.run(port='8443', debug=True, ssl_context='adhoc')
    
        