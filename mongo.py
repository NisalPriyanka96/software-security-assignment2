from dotenv import load_dotenv
from pymongo import *

#config_env = dotenv_values(".env")

class mongo:
    

    def __init__(self, pdatabase):
        self.connectionURL = "mongodb+srv://test:N1996%404%404l@gdrive.okbmg.mongodb.net/googletoken?retryWrites=true"
        self.database = pdatabase

    
    ## return database with connection
    def client(self):
        client =  MongoClient(self.connectionURL)
        db = client[self.database]
        print(str(db))
        return db
    
    ## insert token to database
    def insertToken(self, accesstoken, email, pcollection):

        isrecordexists = self.checkEmailindb(email, pcollection)

        if isrecordexists == True:
            self.updateToken(email, pcollection, accesstoken)
            print("record updated")
        else:
            insert_data = {
            "email":email,
            "token":accesstoken
            } 
            collection = self.client()[pcollection]
            result = collection.insert_one(insert_data)
            print(f"One record inserted: {result.inserted_id}")
    

    ## get access token from collection - using email address
    def readCollection(self, collection, email):
        mycol = self.client()[collection]
        myquery = {"email":email}
        mydoc = mycol.find(myquery)

        result = []
        for x in mydoc:
            result.append(x)
            
        return result

    def updateToken(self, email, collection, access_token):
        mycol = self.client()[collection]
        myquery = {"email":email}
        updateQuery = { "$set": { "token": access_token } }

        mycol.update_one(myquery, updateQuery)

        for x in mycol.find():
            print(x) 

    def checkEmailindb(self,email, collection):
        ## populat with result
        result = self.readCollection(collection,email)

        length = len(result)
        print(length)

        if length > 0:
            return True

        
        return False

    def getToken(self, collection, email):
        mycol = self.client()[collection]
        myquery = {"email":email}
        mydoc = mycol.find(myquery)

        token = ""

        for x in mydoc:
            print(x["token"])
            token = x["token"]

        return token

        
