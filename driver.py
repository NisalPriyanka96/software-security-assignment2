from dotenv.main import dotenv_values
from flask.templating import render_template
import json
from dotenv import load_dotenv
from flask import *
import requests
from mongo import mongo
from pymongo import *
import profile



config_env = dotenv_values(".env")
CLIENT_ID = config_env["app_id"]
CLIENT_SECRET = config_env["app_secret"]

def root():
    return render_template('index.html')

def redirectToGoogle():
    if request.method == 'GET':
        return redirect('https://accounts.google.com/o/oauth2/v2/auth?redirect_uri=https%3A%2F%2Flocalhost%3A8443%2Fdriver%2Fcallback&prompt=consent&response_type=code&client_id='+CLIENT_ID+'&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&access_type=offline')

def getCode():
    if request.method == 'GET':
        code = request.args.get("code")
        
        url = "https://oauth2.googleapis.com/token"
        data = 'redirect_uri=https%3A%2F%2Flocalhost%3A8443%2Fdriver%2Fcallback&client_id='+CLIENT_ID+'&client_secret='+CLIENT_SECRET+'&grant_type=authorization_code&code='+code
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        
        response = requests.post(url, headers=headers, data=data, allow_redirects=True)
        res_json = json.loads(response.text)
        #return redirect('/driver/getToken?code='+code)
        
        return redirect('/driver/getToken?token='+res_json["access_token"], code=307)
    
def getAccessToken():
    if request.method == 'GET':
        token = request.args.get("token")

        ## get useremail
        url = "https://www.googleapis.com/oauth2/v2/userinfo"
        headers = {'Authorization':'Bearer '+token}

        response = requests.get(url, headers=headers)

        print(response.text)
        res_json = json.loads(response.text)
        email = res_json["email"]
        
        ## write to DB
        try:
            p1 = mongo("googletoken")
            p1.insertToken(token,email,"candidate")
        except Exception as e:
            print(e)
            return 'DB error occured!!'

        ## make this redirect
        return redirect('/api/user/dashboard?email='+email, code=302)

def getDashBoard():
    try:
        p1 = mongo("googletoken")
        email = request.args.get("email")
        token = p1.getToken("candidate",email)

        print("Token : "+token)

        resp = make_response(render_template('dashboard.html'))
        resp.set_cookie('userID', email)
        resp.set_cookie('token', token)

        # json_resp = {"email":email, "token":token}

    except:
        return 'Error Occured'

        # return resp
    return resp



def test():
    if request.method == 'GET':
        
        p1 = mongo("googletoken")

        # p1.insertToken("tokenxxdfee","someone@gmail.com","candidate")
        # # email = p1.readCollection("candidate","nisalpriyanka96@gmail.com")
        # # print(email)
        # p1.getToken("candidate", "nisalpriyanka96@gmail.com")
        
        


        return str(profile.getNewsTop())
    