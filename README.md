<!-- GETTING STARTED -->
## Getting Started

The application is build under python3 framework

### Prerequisites

The python libraries included in requirements.txt need to run the application

* pip3

  ```
  pip3 install -r requirements.txt
  ```

### Installation

1. Download .env file using following url.

    <a href = "https://cloud.nic.lk/index.php/s/2p7hxolBEdhnULa">Link To .env</a>

2. Place it to the project root directory.
3. Run application using, python3 server.py
4. The google client application currently is in verification process, so the unsafe to proceed message can be appear.
5. Refer <b>Application_Workflow.pdf</b> to further understanding of the Web application and its workflow.

### Video Tutorial

https://www.youtube.com/watch?v=QLKYhvuog0Q


