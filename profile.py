from flask import *
import requests
import json
from werkzeug.utils import secure_filename
import os

def getProfileImage():
    if request.method == 'POST':
        token = request.form.get('token')

        ## get useremail
        url = "https://www.googleapis.com/oauth2/v2/userinfo"
        headers = {'Authorization':'Bearer '+token}

        response = requests.get(url, headers=headers)

        resp_json = json.loads(response.text)
        print(resp_json)
        picture = resp_json["picture"]

        return picture

def getNewsTop():
    res_news = requests.get("https://newsapi.org/v2/top-headlines?country=us&apiKey=686c37fce5d04601bdc63aa1931b585d")

    json_newRes = json.loads(res_news.text)
    news_Results = json_newRes["articles"]

    print(len(news_Results))

    news_list = []
    for i in range(11):
        # news_list.append(news_Results[i]["title"])
        # news_list.append(news_Results[i]["description"])
        # news_list.append(news_Results[i]["url"])
        # news_list.append(news_Results[i]["urlToImage"])
        json_construct = {'title':news_Results[i]["title"], 'description':news_Results[i]["description"], 'url':news_Results[i]["url"], 'image':news_Results[i]["urlToImage"]}

        news_list.append(json_construct)


    
    return json.dumps(news_list)

## google drive file upload
def upload_file(token, filepath, filename):
    headers = {"Authorization":"Bearer "+token}
    para = {"name": filename}

    files = {'data': ('metadata', json.dumps(para), 'application/json; charset=UTF-8'),'file': open(filepath, "rb")}

    r = requests.post("https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart", headers=headers, files=files)

    print(r.text)


    